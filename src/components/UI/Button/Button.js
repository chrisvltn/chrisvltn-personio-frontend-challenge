import styled from 'styled-jss'

const Button = styled('button')({
	display: 'inline-block',
	cursor: ({ disabled }) => disabled ? 'not-allowed' : 'pointer',
	textAlign: 'center',
	fontSize: 16,
	background: ({ disabled }) => disabled ? '#ececec' : '#61b763',
	boxShadow: ({ disabled }) => (disabled ? 'inset ' : '') + '2px 2px 5px rgba(0, 0, 0, 0.1)',
	height: 32,
	minWidth: 32,
	lineHeight: '32px',
	color: ({ disabled }) => disabled ? '#bdbdbd' : '#ecf0f1',
	transition: 'opacity 0.2s ease',
	verticalAlign: 'middle',
	outline: 'none',
	padding: [0, 15],
	borderRadius: 5,
	border: ({ disabled }) => disabled ? '1px solid #dddddd' : '1px solid #529755',
	'&:hover': {
		opacity: 0.9,
	},
})

export default Button
