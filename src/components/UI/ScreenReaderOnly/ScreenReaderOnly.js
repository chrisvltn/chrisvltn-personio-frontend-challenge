import styled from 'styled-jss'

const ScreenReaderOnly = styled('span')({
	position: 'absolute',
	width: 1,
	height: 1,
	padding: 0,
	overflow: 'hidden',
	clip: 'rect(0, 0, 0, 0)',
	whiteSpace: 'nowrap',
	border: 0,
})

export default ScreenReaderOnly
