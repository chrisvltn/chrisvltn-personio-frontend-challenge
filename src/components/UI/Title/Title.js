/* Libs */
import React from 'react'
import withStyles from 'react-jss'

/* Types */
import type { Node } from 'react'
import type { SvgIcon } from '@material-ui/core'

/* Type Definitions */
type Props = {
	icon: SvgIcon,
	classes?: any,
	children?: Node,
}

const Title = ({
	classes = {},
	icon: Icon,
	children,
}: Props) =>
	<h1 className={classes.title}>
		<Icon className={classes.icon} />
		<span className={classes.text}>{children}</span>
	</h1>

const styles = {
	title: {
		padding: [0, 10],
		margin: { bottom: 20 },
	},
	content: {
		verticalAlign: 'middle',
		lineHeight: '26px',
		display: 'inline-block',
	},
	icon: {
		extend: 'content',
		fontSize: 18,
	},
	text: {
		extend: 'content',
		fontSize: 24,
		marginLeft: 10,
	},
}

export default withStyles(styles)(Title)
