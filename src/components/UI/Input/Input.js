/* Libs */
import React from 'react'
import withStyles from 'react-jss'

/* Types */
import type { SvgIcon } from '@material-ui/core'

/* Type definitions */
type Props = {
	classes: any,
	icon: SvgIcon,
	label: string,
	[key: string]: any,
}

const Input = ({
	icon: Icon,
	label,
	classes,
	...props
}: Props) =>
	<label className={classes.label}>
		<span className={classes.text}>{label}</span>
		<Icon className={classes.icon} />
		<input className={classes.input} {...props} />
	</label>

const styles = {
	label: {
		display: 'block',
		position: 'relative',
		padding: [0, 10],
		margin: { bottom: 10 },
	},
	text: {
		display: 'block',
		fontWeight: 500,
		margin: { bottom: 5 },
	},
	icon: {
		position: 'absolute',
		bottom: 3,
		left: 20,
	},
	input: {
		display: 'block',
		width: '100%',
		borderRadius: 5,
		height: 30,
		lineHeight: '30px',
		fontSize: 16,
		boxSizing: 'border-box',
		border: {
			style: 'solid',
			width: 1,
			color: '#ddd',
		},
		padding: {
			left: 40,
			right: 10,
		},
	},
}

export default withStyles(styles)(Input)
