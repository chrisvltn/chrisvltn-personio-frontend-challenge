import styled from 'styled-jss'

const HorizontalScroll = styled('div')({
	whiteSpace: 'nowrap',
	overflowX: 'auto',
})

export default HorizontalScroll
