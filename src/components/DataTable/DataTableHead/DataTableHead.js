/* Libs */
import React from 'react'

/* Components */
import DataTableHeader from '../DataTableHeader/DataTableHeader';

/* Type definitions */
export type ColumnDefinition = {
	name: string,
	field: string,
	isFilter?: boolean,
	isSortable?: boolean,
	customClass?: (value: any) => string,
	format?: (value: any) => any,
}

type Props = {
	columns: ColumnDefinition[],
	onSort: (field: string) => any,
	sortOrders: {
		[field: string]: 'asc' | 'desc',
	},
}

const DataTableHead = ({
	columns,
	sortOrders,
	onSort,
}: Props) =>
	<thead>
		<tr>
			{columns.map((column, index) =>
				<DataTableHeader
					key={column.field}
					field={column.field}
					name={column.name}
					onSort={onSort}
					order={sortOrders[column.field] || 'asc'}
					sortable={column.isSortable}
					fixed={index === 0}
				/>
			)}
		</tr>
	</thead>

export default DataTableHead
