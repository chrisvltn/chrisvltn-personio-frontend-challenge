/* Libs */
import React, { Component } from 'react'
import _ from 'lodash'

/* Icons */
import { Search } from '@material-ui/icons'

/* Components */
import Input from '../../UI/Input/Input';

/* Type definitions */
type Props = {
	onFilter: (query: string) => any,
	debounce?: number,
}

type State = {
	query: string
}

class DataTableFilterInput extends Component<Props, State> {
	state: State = {
		query: '',
	}

	onFilterChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
		const query = event.target.value
		this.setState({ query })
		this.updateFilter(query)
	}

	/* Debounce is used here to avoid repeating the process of filtering and sorting */
	updateFilter = _.debounce((query: string) => {
		this.props.onFilter(query.toLowerCase())
	}, this.props.debounce || 0)

	render() {
		return (
			<Input label="Filter:" icon={Search} onChange={this.onFilterChange} value={this.state.query} />
		)
	}
}

export default DataTableFilterInput
