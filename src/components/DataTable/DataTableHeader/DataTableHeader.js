/* Libs */
import React from 'react'
import withStyles from 'react-jss'

/* Icons */
import { ArrowDropDown, ArrowDropUp } from '@material-ui/icons'

/* Type Definition */
type Props = {
	classes: any,
	field: string,
	name: string,
	onSort?: (field: string) => any,
	sortable?: boolean,
	fixed?: boolean,
	order?: 'asc' | 'desc'
}

const DataTableHeader = ({
	classes,
	field,
	onSort = () => { },
	sortable,
	name,
	order,
	fixed,
}: Props) => {
	const Icon = order === 'asc' ? ArrowDropDown : ArrowDropUp
	const fixedClass = fixed ? ' ' + classes.fixed : ''

	return (
		<th aria-sort={sortable ? order === 'asc' ? 'ascending' : 'descending' : null} className={classes.tableHeader + fixedClass} scope="col">
			{
				sortable ?
					<button className={classes.content + ' ' + classes.pointer + ' ' + classes.dim} type="button" title={`Sort by ${name}`} onClick={() => onSort(field)}>
						{name}
						<Icon className={classes.icon} />
					</button>
					:
					<div className={classes.content}>{name}</div>
			}
		</th>
	)
}

const styles = {
	tableHeader: {
		padding: 0,
		borderTop: {
			style: 'solid',
			width: 1,
			color: 'rgba(0, 0, 0, 0.2)',
		},
		'&:last-child': {
			borderRight: 0,
		},
	},
	fixed: {
		'@media screen and (max-width: 720px)': {
			position: 'sticky',
			backgroundColor: '#fff',
			boxShadow: '0 2px 5px rgba(0, 0, 0, 0.3)',
			left: 0,
		},
	},
	content: {
		display: 'block',
		boxSizing: 'border-box',
		width: '100%',
		padding: [5, 10],
		border: 0,
		margin: 0,
		background: 'none',
		outline: 'none',
		textAlign: 'left',
		fontWeight: 500,
		fontSize: 14,
		font: 'inherit',
		lineHeight: '24px',
		verticalAlign: 'middle',
	},
	pointer: {
		cursor: 'pointer',
	},
	dim: {
		'transition': 'all 0.2s ease',
		'&:hover': {
			backgroundColor: '#efefef',
		}
	},
	icon: {
		lineHeight: '24px',
		verticalAlign: 'middle',
	},
}

export default withStyles(styles)(DataTableHeader)
