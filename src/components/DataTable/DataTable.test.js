import React from 'react'
import DataTable from './DataTable'
import { MemoryRouter, Router } from 'react-router'
import { mount } from 'enzyme';
import { createBrowserHistory } from 'history';

it('renders in table rows based on provided columns', async () => {
	const columns = [
		{ name: 'Name', field: 'name' },
		{ name: 'Birth Date', field: 'birthDate' },
		{ name: 'Age', field: 'age' },
	]

	const data = [
		{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
		{ id: 2, name: 'John', birthDate: '1874-11-19', age: 21 },
		{ id: 3, name: 'Geovana', birthDate: '1348-11-19', age: '22' },
		{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
		{ id: 5, name: 'Raquel', birthDate: '1365-11-19', age: '25' },
	]

	/* Renders table */
	const container = mount(
		<MemoryRouter>
			<DataTable keyField="id" columns={columns} data={data} />
		</MemoryRouter>
	)

	/* The component creates only 1 table element */
	const table = container.find('table')
	expect(table).toHaveLength(1)

	/* Table element has a table header */
	const thead = table.find('thead')
	expect(thead).toHaveLength(1)

	/* The header has the exact number of columns */
	const headings = thead.find('th')
	expect(headings).toHaveLength(columns.length)

	/* Heading texts should have the column names */
	headings.forEach((th, index) => expect(th.text()).toEqual(columns[index].name))

	/* Table element has table body */
	const tbody = table.find('tbody')
	expect(tbody).toHaveLength(1)

	/* The body has the exact number of rows */
	const rows = tbody.find('tr')
	expect(rows).toHaveLength(data.length)

	/* Rows texts should have the exact data */
	rows.forEach((tr, index) => {
		const headings = tr.find('th')
		const cells = tr.find('td')

		expect(headings).toHaveLength(Math.min(columns.length, 1))
		expect(cells).toHaveLength(Math.max(columns.length - 1, 0))
		expect(headings.text()).toEqual(data[index].name.toString())
		expect(cells.at(0).text()).toEqual(data[index].birthDate.toString())
		expect(cells.at(1).text()).toEqual(data[index].age.toString())
	})
})

const cases = {
	'[INITIAL] filters data based on input': {
		filter: 'h',
		columns: [
			{ name: 'Name', field: 'name', isFilter: true },
			{ name: 'Birth Date', field: 'birthDate' },
			{ name: 'Age', field: 'age' },
		],
		data: [
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 2, name: 'John', birthDate: '1874-11-19', age: 21 },
			{ id: 3, name: 'Geovana', birthDate: '1348-11-19', age: '22' },
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
			{ id: 5, name: 'Raquel', birthDate: '1365-11-19', age: '25' },
		],
		expectedQueryString: '?q=h',
		expectedData: [
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 2, name: 'John', birthDate: '1874-11-19', age: 21 },
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
		]
	},
	'[INITIAL] sorts data based on one field': {
		columns: [
			{ name: 'Name', field: 'name', isSortable: true },
			{ name: 'Birth Date', field: 'birthDate' },
			{ name: 'Age', field: 'age' },
		],
		data: [
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 2, name: 'John', birthDate: '1874-11-19', age: 21 },
			{ id: 3, name: 'Geovana', birthDate: '1348-11-19', age: '22' },
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
			{ id: 5, name: 'Raquel', birthDate: '1365-11-19', age: '25' },
		],
		expectedQueryString: '',
		expectedData: [
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 3, name: 'Geovana', birthDate: '1348-11-19', age: '22' },
			{ id: 2, name: 'John', birthDate: '1874-11-19', age: 21 },
			{ id: 5, name: 'Raquel', birthDate: '1365-11-19', age: '25' },
		]
	},
	'[INITIAL] sorts data based on multiple fields': {
		columns: [
			{ name: 'Name', field: 'name', isSortable: true },
			{ name: 'Birth Date', field: 'birthDate', isSortable: true },
			{ name: 'Age', field: 'age' },
		],
		data: [
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 2, name: 'Chris', birthDate: '1874-11-19', age: 21 },
			{ id: 3, name: 'Chris', birthDate: '1348-11-19', age: '22' },
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
			{ id: 5, name: 'Agatha', birthDate: '1365-11-19', age: '25' },
		],
		expectedQueryString: '',
		expectedData: [
			{ id: 5, name: 'Agatha', birthDate: '1365-11-19', age: '25' },
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
			{ id: 3, name: 'Chris', birthDate: '1348-11-19', age: '22' },
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 2, name: 'Chris', birthDate: '1874-11-19', age: 21 },
		]
	},
	'[INITIAL] sorts and filters data based on multiple fields': {
		filter: 'h',
		columns: [
			{ name: 'Name', field: 'name', isFilter: true, isSortable: true },
			{ name: 'Birth Date', field: 'birthDate', isSortable: true },
			{ name: 'Age', field: 'age' },
		],
		data: [
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 2, name: 'Chris', birthDate: '1874-11-19', age: 21 },
			{ id: 3, name: 'Geovana', birthDate: '1348-11-19', age: '22' },
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
			{ id: 5, name: 'Agatha', birthDate: '1365-11-19', age: '25' },
		],
		expectedQueryString: '?q=h',
		expectedData: [
			{ id: 5, name: 'Agatha', birthDate: '1365-11-19', age: '25' },
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 2, name: 'Chris', birthDate: '1874-11-19', age: 21 },
		]
	},
	'[QUERY STRING] filters data based on input': {
		query: '?q=h',
		columns: [
			{ name: 'Name', field: 'name', isFilter: true },
			{ name: 'Birth Date', field: 'birthDate' },
			{ name: 'Age', field: 'age' },
		],
		data: [
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 2, name: 'John', birthDate: '1874-11-19', age: 21 },
			{ id: 3, name: 'Geovana', birthDate: '1348-11-19', age: '22' },
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
			{ id: 5, name: 'Raquel', birthDate: '1365-11-19', age: '25' },
		],
		expectedQueryString: '?q=h',
		expectedData: [
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 2, name: 'John', birthDate: '1874-11-19', age: 21 },
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
		]
	},
	'[QUERY STRING] sorts data based on one fields by desc': {
		query: '?fields_order=name&sort_order=desc',
		columns: [
			{ name: 'Name', field: 'name', isSortable: true },
			{ name: 'Birth Date', field: 'birthDate' },
			{ name: 'Age', field: 'age' },
		],
		data: [
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 2, name: 'John', birthDate: '1874-11-19', age: 21 },
			{ id: 3, name: 'Geovana', birthDate: '1348-11-19', age: '22' },
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
			{ id: 5, name: 'Raquel', birthDate: '1365-11-19', age: '25' },
		],
		expectedQueryString: '?fields_order=name&sort_order=desc',
		expectedData: [
			{ id: 5, name: 'Raquel', birthDate: '1365-11-19', age: '25' },
			{ id: 2, name: 'John', birthDate: '1874-11-19', age: 21 },
			{ id: 3, name: 'Geovana', birthDate: '1348-11-19', age: '22' },
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
		]
	},
	'[QUERY STRING] sorts data based on multiple fields by asc and desc': {
		query: '?fields_order=name%2CbirthDate&sort_order=asc%2Cdesc',
		columns: [
			{ name: 'Name', field: 'name', isSortable: true },
			{ name: 'Birth Date', field: 'birthDate', isSortable: true },
			{ name: 'Age', field: 'age' },
		],
		data: [
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 3, name: 'Chris', birthDate: '1348-11-19', age: '22' },
			{ id: 2, name: 'Chris', birthDate: '1874-11-19', age: 21 },
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
			{ id: 5, name: 'Agatha', birthDate: '1365-11-19', age: '25' },
		],
		expectedQueryString: '?fields_order=name%2CbirthDate&sort_order=asc%2Cdesc',
		expectedData: [
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
			{ id: 5, name: 'Agatha', birthDate: '1365-11-19', age: '25' },
			{ id: 2, name: 'Chris', birthDate: '1874-11-19', age: 21 },
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 3, name: 'Chris', birthDate: '1348-11-19', age: '22' },
		]
	},
	'[QUERY STRING] sorts and filters data based on multiple fields by desc and asc': {
		query: '?q=h&fields_order=name%2CbirthDate&sort_order=desc%2Casc',
		columns: [
			{ name: 'Name', field: 'name', isFilter: true, isSortable: true },
			{ name: 'Birth Date', field: 'birthDate', isSortable: true },
			{ name: 'Age', field: 'age' },
		],
		data: [
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 2, name: 'Chris', birthDate: '1874-11-19', age: 21 },
			{ id: 3, name: 'Geovana', birthDate: '1348-11-19', age: '22' },
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
			{ id: 5, name: 'Agatha', birthDate: '1365-11-19', age: '25' },
		],
		expectedQueryString: '?q=h&fields_order=name%2CbirthDate&sort_order=desc%2Casc',
		expectedData: [
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 2, name: 'Chris', birthDate: '1874-11-19', age: 21 },
			{ id: 5, name: 'Agatha', birthDate: '1365-11-19', age: '25' },
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
		]
	},
	'[QUERY STRING] sorts by unsortable field': {
		query: '?fields_order=name%2Cage&sort_order=desc%2Cdesc',
		columns: [
			{ name: 'Name', field: 'name', isFilter: true, isSortable: true },
			{ name: 'Birth Date', field: 'birthDate', isSortable: true },
			{ name: 'Age', field: 'age' },
		],
		data: [
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
			{ id: 3, name: 'Geovana', birthDate: '1348-11-19', age: '22' },
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 5, name: 'Agatha', birthDate: '1365-11-19', age: '25' },
			{ id: 2, name: 'Chris', birthDate: '1874-11-19', age: 21 },
		],
		expectedQueryString: '?fields_order=name%2Cage&sort_order=desc%2Cdesc',
		expectedData: [
			{ id: 3, name: 'Geovana', birthDate: '1348-11-19', age: '22' },
			{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
			{ id: 2, name: 'Chris', birthDate: '1874-11-19', age: 21 },
			{ id: 5, name: 'Agatha', birthDate: '1365-11-19', age: '25' },
			{ id: 4, name: 'Agatha', birthDate: '7649-11-19', age: 23 },
		]
	},
}

Object.entries(cases)
	.forEach(([name, input]) => {
		it(name, async () => {
			const {
				query,
				columns,
				data,
				expectedData,
				expectedQueryString,
				filter,
			} = input

			const history = createBrowserHistory()
			history.push(query || '')

			/* Renders table */
			const container = mount(
				<Router history={history}>
					<DataTable keyField="id" columns={columns} data={data} />
				</Router>
			)

			if (filter) {
				/* Simulates search for "is" */
				const input = container.find('input')
				input.simulate('change', { target: { value: filter } });

				/* Waits for debounce */
				await new Promise(r => setTimeout(r, 1000))
				container.update()
			}

			const table = container.find('table')
			const tbody = table.find('tbody')
			const rows = tbody.find('tr')

			/* Checks if the columns have the right value */
			rows.forEach((row, index) => {
				const heading = row.find('th')
				const cols = row.find('td')
				expect(heading.text()).toBe(expectedData[index].name.toString())
				expect(cols.at(0).text()).toBe(expectedData[index].birthDate.toString())
				expect(cols.at(1).text()).toBe(expectedData[index].age.toString())
			})

			/* Checks if the query string is the expected one */
			expect(history.location.search).toBe(expectedQueryString)
		})
	})
