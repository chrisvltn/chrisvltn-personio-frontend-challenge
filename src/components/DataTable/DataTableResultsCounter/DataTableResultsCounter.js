/* Libs */
import React from 'react'
import styled from 'styled-jss'

/* Type definitions */
type Props = {
	counter: number
}

const DataTableResultsCounter = ({
	counter,
}: Props) =>
	<ResultsCounter>
		{counter} {counter > 1 ? 'results were' : 'result was'} found
	</ResultsCounter>

const ResultsCounter = styled('p')({
	fontSize: 16,
	padding: [0, 15],
})

export default DataTableResultsCounter
