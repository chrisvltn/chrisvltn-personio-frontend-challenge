import React from 'react'
import DataTableBody from './DataTableBody'
import { MemoryRouter } from 'react-router'
import { mount } from 'enzyme';

it('renders rows based on provided columns and data and sanitize content', async () => {
	const columns = [
		{ name: 'Name', field: 'name' },
		{ name: 'Birth Date', field: 'birthDate' },
		{ name: 'Age', field: 'age' },
	]

	const data = [
		{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
		{ id: 2, name: 'John', birthDate: '1874-11-19', age: 21 },
		{ id: 5, name: 'Raquel', birthDate: '1365-11-19', age: '25' },

		/* Unsafe strings should be sanitized */
		{ id: 3, name: 'Geov&ana', birthDate: '1348-11-19', age: '22' },
		{ id: 4, name: '<script>alert("Hi!");</script>', birthDate: '7649-11-19', age: 23 },
	]

	const expectedData = [
		{ id: 1, name: 'Chris', birthDate: '1678-11-19', age: 20 },
		{ id: 2, name: 'John', birthDate: '1874-11-19', age: 21 },
		{ id: 5, name: 'Raquel', birthDate: '1365-11-19', age: '25' },
		{ id: 3, name: 'Geov&amp;ana', birthDate: '1348-11-19', age: '22' },
		{ id: 4, name: '&lt;script&gt;alert(&#34;Hi!&#34;);&lt;/script&gt;', birthDate: '7649-11-19', age: 23 },
	]

	/* Renders table */
	const container = mount(
		<MemoryRouter>
			<table>
				<DataTableBody keyField="id" columns={columns} data={data} />
			</table>
		</MemoryRouter>
	)

	/* Should create a single table body */
	const tbody = container.find('tbody')
	expect(tbody).toHaveLength(1)

	/* Should have 1 heading per row */
	const headings = tbody.find('th')
	expect(headings).toHaveLength(data.length)

	/* The body has the exact number of rows */
	const rows = tbody.find('tr')
	expect(rows).toHaveLength(data.length)

	/* Rows texts should have the exact data */
	rows.forEach((row, index) => {
		const headings = row.find('th')
		const cells = row.find('td')

		expect(headings).toHaveLength(Math.min(columns.length, 1))
		expect(cells).toHaveLength(Math.max(columns.length - 1, 0))

		expect(headings.text()).toEqual(expectedData[index].name.toString())
		expect(cells.at(0).text()).toEqual(expectedData[index].birthDate.toString())
		expect(cells.at(1).text()).toEqual(expectedData[index].age.toString())
	})
})
