/* Libs */
import React from 'react'
import withStyles from 'react-jss'
import { escape } from 'sanitizer'

/* Types */
import type { ColumnDefinition } from '../DataTableHead/DataTableHead'

/* Components */
import DataTableCell from '../DataTableCell/DataTableCell';

/* Type definitions */
type Props = {
	classes: any,
	keyField: string,
	columns: ColumnDefinition[],
	data: any[],
}

const DataTableBody = ({
	classes,
	keyField,
	columns,
	data,
}: Props) =>
	<tbody>
		{data.map((row, index) =>
			<tr className={classes.row} key={row[keyField]}>
				{columns.map(({ field, format, customClass }, colIndex) =>
					<DataTableCell
						heading={colIndex === 0}
						fixed={colIndex === 0}
						key={`${row[keyField]}-${field}`}
						className={typeof customClass === 'function' ? customClass(row[field]) : customClass}
					>
						{escape(format ? format(row[field]) : row[field])}
					</DataTableCell>
				)}
			</tr>
		)}
	</tbody>

const styles = {
	row: {
		'&:hover': {
			transition: 'all 0.2s ease',
			transform: 'translateY(-2px)',
			boxShadow: '2px 2px 5px rgba(0, 0, 0, 0.2)',
		},
	},
}

export default withStyles(styles)(DataTableBody)
