/* Libs */
import React from 'react'
import withStyles from 'react-jss'

/* Type definitions */
type Props = {
	heading: boolean,
	fixed: boolean,
	className?: string,
	classes: any,
}

const DataTableCell = ({
	classes,
	className,
	heading,
	fixed,
	...props
}: Props) => {
	const classList = [classes.col]

	if (className) classList.push(className)
	if (fixed) classList.push(classes.fixed)

	const colClass = classList.join(' ')

	return (
		heading ?
			<th className={colClass} scope="row" {...props} /> :
			<td className={colClass} {...props} />
	)
}

const styles = {
	col: {
		fontSize: 14,
		fontWeight: 400,
		textAlign: 'left',
		padding: 10,
		borderTop: {
			style: 'solid',
			width: 1,
			color: 'rgba(0, 0, 0, 0.2)',
		},
		'&:last-child': {
			borderRight: 0,
		},
	},
	fixed: {
		'@media screen and (max-width: 720px)': {
			position: 'sticky',
			backgroundColor: '#fff',
			boxShadow: '0 2px 5px rgba(0, 0, 0, 0.3)',
			left: 0,
		},
	},
}

export default withStyles(styles)(DataTableCell)
