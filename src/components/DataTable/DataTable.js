/* Libs */
import React, { Component } from 'react'
import styled from 'styled-jss'
import _ from 'lodash'
import { withRouter } from 'react-router-dom';
import { parse, stringify } from 'query-string'

/* Types */
import type { ContextRouter } from 'react-router'
import type { ColumnDefinition } from './DataTableHead/DataTableHead'

/* Components */
import DataTableHead from './DataTableHead/DataTableHead'
import DataTableBody from './DataTableBody/DataTableBody'
import DataTableFilterInput from './DataTableFilterInput/DataTableFilterInput'
import HorizontalScroll from '../UI/HorizontalScroll/HorizontalScroll';
import Pagination from '../Pagination/Pagination';
import DataTableResultsCounter from './DataTableResultsCounter/DataTableResultsCounter';
import ScreenReaderOnly from '../UI/ScreenReaderOnly/ScreenReaderOnly';

/* Type definitions */
type Props = {
	...ContextRouter,
	key?: any, /* The key is required when `props` changes to avoid rendering with the wrong state */
	keyField?: string,
	name: string,
	columns: ColumnDefinition[],
	data: any[],
	perPage: number,
	paginated?: boolean,
}

type State = {
	query: string,
	filterFields: string[],
	sortFields: string[],
	sortOrder: ('asc' | 'desc')[],
	currentList: any[],
	paginatedList: any[],
	page: number,
	maxPages: number,
}

class DataTable extends Component<Props, State> {
	state: State = {
		query: '',
		filterFields: [],
		sortFields: [],
		sortOrder: [],
		currentList: [],
		paginatedList: [],
		page: 1,
		maxPages: 1,
	}

	static defaultProps = {
		perPage: 30,
	}

	componentWillMount() {
		/* Organizes initial state */
		const {
			sortFields,
			filterFields,
			currentList,
			sortOrder,
		} = this.propsToState(this.props)

		/* Checks query string filters and sorting orders */
		const { q, page, fields_order, sort_order } = parse(this.props.location.search) || {}
		const queryFieldsOrder = _.toString(fields_order).split(',')
		const querySortOrder = _.toString(sort_order).split(',')
		const query: string = _.toString(q) || ''

		for (let index = queryFieldsOrder.length - 1; index >= 0; index--) {
			const field = queryFieldsOrder[index]

			/* As the user controls the query string, we need to check if the field actually exists and is sortable */
			if (!_.includes(sortFields, field)) continue

			/* Removes the field from fields and order arrays */
			_.pull(sortFields, field)
			sortOrder.pop()

			/* Reinserts them at the beginning of the array, so it will have priority */
			const order: 'asc' | 'desc' = (querySortOrder[index]: any);
			sortFields.unshift(field)
			sortOrder.unshift(order === 'desc' ? 'desc' : 'asc')
		}

		this.setState({
			page: parseInt(page) || 1,
			query,
			sortFields,
			filterFields,
			sortOrder,
			currentList,
		})

		this.filterList(this.props.data)
	}

	propsToState = (props: Props): State => {
		const sortFields = _.chain(this.props.columns).filter('isSortable').map('field').value()
		const filterFields = _.chain(this.props.columns).filter('isFilter').map('field').value()
		const currentList = _.orderBy(this.props.data, sortFields)
		const sortOrder = sortFields.map(() => 'asc')

		return {
			query: '',
			sortFields,
			filterFields,
			currentList,
			sortOrder,
			paginatedList: currentList,
			maxPages: 0,
			page: 1,
		}
	}

	filterList = (list: any[] = this.props.data) => {
		this.setState(state => {
			const newList = !state.query ? list : _.filter(list, rowData =>
				_.chain(rowData)
					.at(state.filterFields)
					.some(value => _.includes(value.toLowerCase(), state.query))
					.value())

			return {
				maxPages: Math.ceil(newList.length / this.props.perPage),
				currentList: newList,
			}
		})

		this.sortList()
	}

	sortList = () => {
		this.setState(state => {
			const newList = _.orderBy(state.currentList, state.sortFields, state.sortOrder)
			return {
				currentList: newList,
			}
		})

		this.paginateList()
	}

	toggleSortOrder = (field: string) => {
		/* Toggle state of the selected field */
		this.setState(state => {
			const index = state.sortFields.indexOf(field)
			const newFields = state.sortFields.concat()
			const newOrder = state.sortOrder.concat()

			/* Puts the field at the beginning of the array, so it will have priority */
			newFields.unshift(newFields.splice(index, 1)[0])
			newOrder.unshift(newOrder.splice(index, 1)[0] === 'asc' ? 'desc' : 'asc')

			this.updateLocationQueryString({
				fields_order: newFields.join(','),
				sort_order: newOrder.join(','),
			})

			return {
				sortFields: newFields,
				sortOrder: newOrder,
			}
		})

		this.sortList()
	}

	updateQuery = (query: string) => {
		this.setState({ query })
		this.updateLocationQueryString({ q: query })
		this.filterList()
	}

	updatePage = (page: number) => {
		this.setState({ page })
		this.updateLocationQueryString({ page: page.toString() })
		this.paginateList()
	}

	paginateList = () => {
		this.setState(state => {
			const { page, currentList } = state

			if (!this.props.paginated)
				return {
					paginatedList: currentList
				}

			/* As the user can set the page through the query string, we should check if the page is actually accessible */
			const currentPage = Math.max(Math.min(state.maxPages, page), 1)

			const paginatedList = currentList.slice((currentPage - 1) * this.props.perPage, currentPage * this.props.perPage)

			return {
				page: currentPage,
				paginatedList,
			}
		})
	}

	updateLocationQueryString(fields: { [key: string]: string }) {
		const query: any = parse(this.props.location.search)

		for (let field in fields)
			query[field] = fields[field]

		this.props.history.push('?' + stringify(query))
	}

	render() {
		const list = this.state.paginatedList
		const sorting = _.zipObject(this.state.sortFields, this.state.sortOrder)

		return (
			<div>
				<DataTableFilterInput debounce={800} onFilter={this.updateQuery} />
				<DataTableResultsCounter counter={this.state.currentList.length} />
				<HorizontalScroll>
					<Table>
						<caption>
							<ScreenReaderOnly>
								{this.props.name}
							</ScreenReaderOnly>
						</caption>

						<DataTableHead columns={this.props.columns} sortOrders={sorting} onSort={this.toggleSortOrder} />
						<DataTableBody keyField={this.props.keyField} columns={this.props.columns} data={list} />
					</Table>
				</HorizontalScroll>

				{this.props.paginated ?
					<Pagination onChange={this.updatePage} page={this.state.page} maxPages={this.state.maxPages} />
					: null}

			</div>
		)
	}
}

const Table = styled('table')({
	border: 0,
	borderCollapse: 'separate',
	borderSpacing: 0,
	width: '100%',
})

export default withRouter(DataTable)
