/* Libs */
import React from 'react'
import withStyles from 'react-jss'

/* Icons */
import { FirstPage, LastPage } from '@material-ui/icons'

/* Components */
import Button from '../UI/Button/Button';

/* Type definitions */
type Props = {
	classes: any,
	onChange: (page: number) => any,
	page: number,
	maxPages: number,
}

const Pagination = ({
	classes,
	onChange,
	page,
	maxPages,
}: Props) =>
	<div className={classes.wrapper}>
		<nav role="navigation" aria-label="Pagination Navigation">
			<Button className={classes.iconButton} disabled={page === 1} onClick={() => onChange(1)} aria-label="Goto first page">
				<FirstPage className={classes.icon} />
			</Button>

			<Button className={classes.button + (page - 1 === 0 ? ' ' + classes.invisible : '')} onClick={() => onChange(page - 1)} aria-label={`Goto Page ${page - 1}`}>
				{page - 1}
			</Button>

			<Button className={classes.currentPage} disabled aria-label={`Current Page, Page ${page}`} aria-current="true">
				{page}
			</Button>

			<Button className={classes.button + (page + 1 > maxPages ? ' ' + classes.invisible : '')} onClick={() => onChange(page + 1)} aria-label={`Goto Page ${page + 1}`}>
				{page + 1}
			</Button>

			<Button className={classes.iconButton} disabled={page === maxPages} onClick={() => onChange(maxPages)} aria-label="Goto last page">
				<LastPage className={classes.icon} />
			</Button>
		</nav>
	</div>

const styles = {
	wrapper: {
		textAlign: 'center',
		verticalAlign: 'middle',
		marginTop: 20,
	},
	button: {
		background: 'none',
		borderRadius: 0,
		color: 'black',
		border: '1px solid #eee',
		fontSize: 16,
		boxShadow: 'none',
		'&[disabled]': {
			background: '#ededed',
		}
	},
	invisible: {
		visibility: 'hidden',
	},
	currentPage: {
		extend: 'button',
	},
	iconButton: {
		extend: 'button',
		padding: [0, 10],
	},
	icon: {
		maxWidth: 20,
		lineHeight: '32px',
		verticalAlign: 'middle',
	},
}

export default withStyles(styles, { index: 1 })(Pagination)
