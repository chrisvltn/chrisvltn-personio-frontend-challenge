/* Libs */
import React from 'react'
import withStyles from 'react-jss'
import Button from '../UI/Button/Button';

/* Type definitions */
type Props = {
	classes: any,
	message: string,
	onTryAgain: Function,
}

const ErrorMessage = ({
	classes,
	message,
	onTryAgain,
}: Props) =>
	<div className={classes.wrapper}>
		<span className={classes.message}>{message}</span>
		<Button className={classes.tryAgain} type="button" onClick={onTryAgain}>
			Try again
		</Button>
	</div>

const styles = {
	wrapper: {
		textAlign: 'center',
	},
	message: {
		display: 'block',
		fontSize: 18,
		marginBottom: 15,
	},
}

export default withStyles(styles)(ErrorMessage)
