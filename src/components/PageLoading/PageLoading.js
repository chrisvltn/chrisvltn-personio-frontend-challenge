/* Libs */
import React from 'react'
import withStyles from 'react-jss'

/* Icon */
import { CircularProgress } from '@material-ui/core';

/* Custom components */
import Container from '../UI/Container/Container';
import ErrorMessage from '../ErrorMessage/ErrorMessage';

type Props = {
	classes: any,
	isLoading: boolean,
	error: string,
}

const refreshPage = () => window.open(window.location.href, '_self')

/**
 * Displays loading spinner while the page is loading and "Try again" button when it fails to load
 * Use it with `react-loadable` in the `loading` parameter
 */
const PageLoading = ({
	classes,
	isLoading,
	error,
}: Props) => {
	const element = isLoading ?
		<CircularProgress /> :
		error ?
			<ErrorMessage
				message="The page couldn't be loaded. Please try again later."

				/* In a larger app, `onTryAgain` would despatch a message to Redux store to try to load the failed component again */
				onTryAgain={refreshPage} />
			: null

	return (
		<Container className={classes.wrapper}>
			{element}
		</Container>
	)
}

const styles = {
	wrapper: {
		textAlign: 'center',
	},
}

export default withStyles(styles)(PageLoading)
