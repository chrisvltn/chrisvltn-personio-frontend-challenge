/* Libs */
import React from 'react';
import Loadable from 'react-loadable'
import { Switch, Route, Redirect } from 'react-router';

/* Components */
import PageLoading from './components/PageLoading/PageLoading';

/* Routes */
/* Here I used code splitting to simulate a real app scenario. In an app with one main component, it would actually not be needed.  */
const AsyncApplicationsPage = Loadable({
	loader: () => import('./routes/ApplicationsPage/ApplicationsPage'),
	loading: PageLoading,
})

const App = () =>
	<div>
		{/* Routing helps to manage the query string inside the `DataTable` component */}
		<Switch>
			<Route exact path="/applications" component={AsyncApplicationsPage} />
			<Redirect from="/" to="/applications" exact />
		</Switch>
	</div>

export default App;
