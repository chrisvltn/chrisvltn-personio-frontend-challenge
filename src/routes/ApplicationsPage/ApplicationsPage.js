/* Libs */
import React, { Component } from 'react';
import jss from '../../lib/jss'
import axios from 'axios'

/* Types */
import type { ColumnDefinition } from '../../components/DataTable/DataTableHead/DataTableHead'

/* Icons */
import { Person } from '@material-ui/icons';
import { CircularProgress } from '@material-ui/core';

/* Components */
import Container from '../../components/UI/Container/Container'
import Title from '../../components/UI/Title/Title'
import ErrorMessage from '../../components/ErrorMessage/ErrorMessage'
import DataTable from '../../components/DataTable/DataTable'

/* Type definitions */
type Application = {
	application_date: string,
	birth_date: string,
	email: string,
	id: number,
	name: string,
	position_applied: string,
	status: string,
	year_of_experience: number,
}

type Props = {
	classes: any,
	axios: any,
}

type State = {
	table: {
		key: any,
		data: Application[],
	},
	error: ?string,
	hasError: boolean,
	isLoading: boolean,
	cancelFetch: Function,
}

/* CSS Classes */
const styles = {
	'green': {
		color: '#27ae60',
	},
	'red': {
		color: '#c0392b',
	},
}

const { classes } = jss
	.createStyleSheet(styles)
	.attach()

class ApplicationsPage extends Component<Props, State> {
	state: State = {
		isLoading: true,
		hasError: false,
		error: null,
		cancelFetch: () => { },
		table: {
			key: null,
			data: [],
		}
	}

	static defaultProps = {
		/* Axios is exposed here so it can be mocked when the automation tests run */
		axios: axios,
	}

	static TABLE_COLUMNS: ColumnDefinition[] = [
		{
			name: 'Name',
			field: 'name',
			isFilter: true,
		},
		{
			name: 'Email',
			field: 'email',
		},
		{
			name: 'Age',
			field: 'birth_date',
			format: (date: string) => {
				const [year, month, day] = date.split('-')
				const birthDate = new Date(parseInt(year), parseInt(month) - 1, parseInt(day))
				const today = new Date()

				const MILISECONDS_IN_YEAR = 1000 * 60 * 60 * 24 * 30 * 12
				return Math.floor(((today.getTime() - birthDate.getTime()) / MILISECONDS_IN_YEAR))
			},
		},
		{
			name: 'Years of Exp.',
			field: 'year_of_experience',
			isSortable: true,
		},
		{
			name: 'Position',
			field: 'position_applied',
			isSortable: true,
			isFilter: true,
		},
		{
			name: 'Applied',
			field: 'application_date',
			isSortable: true,
			format: (date: string) => {
				const [year, month, day] = date.split('-')
				return `${day}/${month}/${year}`
			},
		},
		{
			name: 'Status',
			field: 'status',
			isFilter: true,
			customClass: (status: string) => {
				switch (status) {
					case 'approved':
						return classes.green
					case 'rejected':
						return classes.red
					default:
						return ''
				}
			}
		},
	]

	componentDidMount() {
		this.fetchApplications()
	}

	fetchApplications = async () => {
		const cancelSource = axios.CancelToken.source()

		this.setState({
			isLoading: true,
			hasError: false,
			error: null,
			cancelFetch: cancelSource.cancel.bind(cancelSource)
		})

		let applications = []
		let error = null

		try {
			const { data } = await this.props.axios.get('https://personio-fe-test.herokuapp.com/api/v1/candidates', {
				validateStatus: () => true, /* Responses with status 500 will be able to pass as well, so we can create a better message to the user */
				cancelToken: cancelSource.token,
			})

			/* The server can return an error even with status 200 */
			if (data.error) error = data.error.message

			applications = data.data
		} catch (e) {
			/* Request canceled by the user or component unmounted */
			if (axios.isCancel(e)) return

			/* If the request fails with status 4xx or 5xx */
			error = e.message
		}

		if (error) {
			this.setState({
				cancelFetch: () => { },
				isLoading: false,
				hasError: true,
				error,
			})
		} else {
			this.setState({
				cancelFetch: () => { },
				isLoading: false,
				table: {
					data: applications,
					key: Date.now(),
				},
			})
		}
	}

	componentWillUnmount() {
		/* It may display a warning in tests if the state changes after component unmount */
		this.state.cancelFetch('Component unmounted.')
	}

	render() {
		return (
			<Container>
				<Title icon={Person}>
					Applications
				</Title>

				{this.state.isLoading ?
					<CircularProgress />
					:
					this.state.error ?
						<ErrorMessage message={this.state.error} onTryAgain={this.fetchApplications} />
						:
						<DataTable
							name="List of applicants"
							key={this.state.table.key}
							columns={ApplicationsPage.TABLE_COLUMNS}
							keyField="id"
							data={this.state.table.data}
							paginated
							perPage={10}
						/>
				}
			</Container>
		)
	}
}

export default ApplicationsPage
