import React from 'react'
import ApplicationsPage from './ApplicationsPage'
import { MemoryRouter } from 'react-router'
import { JssProvider } from 'react-jss'
import { mount } from 'enzyme';
import ErrorMessage from '../../components/ErrorMessage/ErrorMessage';
import jss from '../../lib/jss'

it('handles and shows error message', async () => {
	const axios = {
		get: () => Promise.resolve({ data: { error: { code: 500, message: 'An error happened!' } } })
	}

	/* Renders table */
	const container = mount(
		<JssProvider jss={jss}>
			<MemoryRouter>
				<ApplicationsPage axios={axios} />
			</MemoryRouter>
		</JssProvider>
	)

	/* Waits for the request to be called */
	await new Promise(r => setTimeout(r, 500))
	container.update()

	/* Should create a single table body */
	const message = container.find(ErrorMessage)
	expect(message).toHaveLength(1)

	/* The message should have appeared */
	expect(message.find('span').text()).toBe('An error happened!')

	/* Button to try again should exist */
	expect(message.find('button')).toHaveLength(1)
})
