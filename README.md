# [Personio FrontEnd Challenge]

## Installation

### `Step 1` - clone the repo

```bash
$ git clone https://bitbucket.org/chrisvltn/chrisvltn-personio-frontend-challenge.git
```

### `Step 2` - cd in the repo

```bash
$ cd chrisvltn-personio-frontend-challenge
```

### `Step 3` - install dependencies

```bash
$ npm install
```

### `Step 4` - run application

```bash
$ npm start
```

In browser, open http://localhost:3000

## Testing

```bash
$ npm run test
```

## Built with

- [x] - [React]
- [x] - [Flow] - Type checking
- [x] - [JSS] - Styles
- [x] - [Enzyme] - Automated tests

## Main Features

- [x] - Table generated with the API data
- [x] - API error handler
- [x] - Filtering by multiple and dynamic fields
- [x] - Sorting by multiple and dynamic fields
- [x] - URL update after any change at the filter/sort

----------------------------------------------

- [x] - Responsiveness
- [x] - Pagination
- [x] - Accessibility
- [x] - XSS-safe
- [x] - Automated tests

## Credits

- [chrisvltn] - Coding
- [Personio] - Awesome challenge proposal

[Personio FrontEnd Challenge]: https://bitbucket.org/chrisvltn/chrisvltn-personio-frontend-challenge/src
[React]: https://reactjs.org/
[Flow]: https://flow.org
[Enzyme]: https://airbnb.io/enzyme/
[JSS]: https://cssinjs.org/
[chrisvltn]: https://github.com/chrisvltn
[Personio]: https://github.com/chrisvltn
